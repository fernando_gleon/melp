
from __future__ import unicode_literals
import uuid
from django.db import models

# Create your models here.



class Restaurante(models.Model):
    RATING_CHOICES = (
        (0, '0'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
    )    
    id     = models.TextField(primary_key=True, default=uuid.uuid4)
    rating = models.IntegerField(choices=RATING_CHOICES, blank=True, null=True)
    name   = models.TextField(blank=True, null=True)
    site   = models.TextField(blank=True, null=True)
    email  = models.TextField(blank=True, null=True)
    phone  = models.TextField(blank=True, null=True)
    street = models.TextField(blank=True, null=True)
    city   = models.TextField(blank=True, null=True)
    state  = models.TextField(blank=True, null=True)
    lat    = models.TextField(blank=True, null=True)
    lng    = models.TextField(blank=True, null=True)

    # class Meta:
    #     managed = False
    #     db_table = 'restaurantes'

    def __str__(self):
        return '{}'.format(self.name)

