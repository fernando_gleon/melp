from django.conf.urls import url, include
from django.views.generic.base import TemplateView

from .views import RestauranteList, RestauranteDetail

urlpatterns = [

    url(r'^api/restaurantes/$', RestauranteList.as_view(), 
                                name='restaurante-list' ),
    url(r'^api/restaurantes/(?P<pk>[0-9A-Fa-f-]+)/$', RestauranteDetail.as_view(), 
                                name='restaurante-detail' ),

]