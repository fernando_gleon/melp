from rest_framework import serializers
from .models import Restaurante

class RestauranteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurante
        fields = ( 'id', 'rating', 'name', 'site', 'email', 'phone',
                   'street', 'city', 'state', 'lat', 'lng'  )  

