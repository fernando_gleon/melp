from django.shortcuts import render
from rest_framework import generics
from .models import Restaurante
from .serializers import RestauranteSerializer

# Create your views here.

class RestauranteList( generics.ListCreateAPIView):
    queryset = Restaurante.objects.all()
    serializer_class = RestauranteSerializer


class RestauranteDetail( generics.RetrieveUpdateDestroyAPIView):
    queryset = Restaurante.objects.all()
    serializer_class = RestauranteSerializer    